# Various GitLab utility scripts

Various GitLab utility scripts which I've found I needed when dealing with day-to-day tasks.

The main application is at
https://gitlab-utils-uis-devops-experiments-rjw57-f084949dc385254d267f0.uniofcam.dev/.
