export const GITLAB_BASE_URL = "https://gitlab.developers.cam.ac.uk/";
export const GRAPHQL_ENDPOINT = `${GITLAB_BASE_URL}api/graphql`;
export const UIS_DEVOPS_GROUP_FULL_PATH = "uis/devops";
export const PROJECT_FACTORY_TOPIC = "project-factory";
