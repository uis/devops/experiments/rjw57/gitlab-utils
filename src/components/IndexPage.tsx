import Typography from "@mui/material/Typography";

import Page from "./Page";

export const IndexPage = () => {
  return (
    <Page>
      <Typography variant="body1">
        Various little GitLab utilities.
      </Typography>
    </Page>
  );
};

export default IndexPage;
