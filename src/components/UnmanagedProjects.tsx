import React from "react";

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Link from "@mui/material/Link";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Typography from "@mui/material/Typography";

import { gql, request } from "graphql-request";
import { unparse as csvUnparse } from "papaparse";

import Page from "./Page";

import useGitLabAuth from "../hooks/useGitLabAuth";
import {
  GRAPHQL_ENDPOINT,
  UIS_DEVOPS_GROUP_FULL_PATH,
  PROJECT_FACTORY_TOPIC,
} from "../config";

interface ProjectNode {
  id: string;
  name: string;
  fullPath: string;
  webUrl: string;
  topics: string[];
}

interface GroupProjectsQueryData {
  group: {
    projects: {
      nodes: ProjectNode[];
      pageInfo: {
        endCursor: string;
        hasNextPage: boolean;
      };
    };
  };
}

const groupProjectsQuery = gql`
  query groupProjects($fullPath: ID!, $after: String) {
    group(fullPath: $fullPath) {
      projects(after: $after, includeSubgroups: true, includeArchived: false) {
        nodes {
          id
          name
          fullPath
          webUrl
          topics
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`;

const makeCsv = (projectNodes: ProjectNode[]) => {
  const rows = projectNodes.map(({ fullPath, name }) => [fullPath, name]);
  rows.unshift(["Full path", "Name"]);
  return csvUnparse(rows);
};

const makeMarkdownTable = (projectNodes: ProjectNode[]) => {
  const rows = projectNodes.map(
    ({ fullPath, name, webUrl }) =>
      `|[${fullPath}](${webUrl})|${name}|`,
  );
  rows.unshift("|Full Path|Name|", "|-|-|");
  return rows.join("\n");
};

export const UnmanagedProjects = () => {
  const [isSearching, setIsSearching] = React.useState(false);
  const [searchState, setSearchState] = React.useState<React.ReactNode>("");
  const [totalProjectCount, setTotalProjectCount] = React.useState(0);
  const [unmanagedProjectNodes, setUnmanagedProjectNodes] = React.useState<
    ProjectNode[] | null
  >(null);
  const { token } = useGitLabAuth();

  const search = async () => {
    setIsSearching(true);
    setTotalProjectCount(0);
    setUnmanagedProjectNodes(null);
    setSearchState(<>Starting &hellip;</>);
    const detectedUnmanagedProjectNodes: ProjectNode[] = [];
    try {
      let after: string | null = null;
      let totalFound = 0;
      while (true) {
        const response: GroupProjectsQueryData = await request(
          GRAPHQL_ENDPOINT,
          groupProjectsQuery,
          {
            fullPath: UIS_DEVOPS_GROUP_FULL_PATH,
            after,
          },
          {
            Authorization: `Bearer ${token}`,
          },
        );

        totalFound += response.group.projects.nodes.length;
        setTotalProjectCount(totalFound);
        after = response.group.projects.pageInfo.endCursor;
        response.group.projects.nodes.forEach((node) => {
          if (!node.topics.includes(PROJECT_FACTORY_TOPIC)) {
            detectedUnmanagedProjectNodes.push(node);
          }
        });
        setUnmanagedProjectNodes(
          detectedUnmanagedProjectNodes.toSorted((a, b) =>
            a.fullPath.localeCompare(b.fullPath),
          ),
        );
        setSearchState(<>Scanned {totalFound} projects.</>);

        if (!response.group.projects.pageInfo.hasNextPage) {
          break;
        }
      }
      setSearchState("");
    } catch (err) {
      setSearchState(`Search failed: {err}`);
    } finally {
      setIsSearching(false);
    }
  };

  const downloadCsv = () => {
    if (unmanagedProjectNodes === null) {
      return;
    }
    const csvContents = makeCsv(unmanagedProjectNodes);
    const csvBlob = new Blob([csvContents], {
      type: "text/csv;charset=utf-8;",
    });
    var csvUrl = URL.createObjectURL(csvBlob);
    var downloadAnchor = document.createElement("a");
    downloadAnchor.href = csvUrl;
    downloadAnchor.setAttribute("download", "unmanaged_projects.csv");
    downloadAnchor.click();
  };

  const copyMarkdownTable = async () => {
    if (unmanagedProjectNodes === null) {
      return;
    }
    const markdownTable = makeMarkdownTable(unmanagedProjectNodes);
    await navigator.clipboard.writeText(markdownTable);
  };

  return (
    <Page title="Projects not managed by the project factory">
      <Typography variant="body1" sx={{ marginBottom: 2 }}>
        Lists projects in GitLab under <code>uis/devops</code> which are not
        archived or managed by the GitLab project factory.
      </Typography>
      <Box sx={{ display: "flex", alignItems: "baseline" }} gap={1} mb={2}>
        <Button disabled={isSearching} variant="outlined" onClick={search}>
          Search
        </Button>
        {!isSearching && unmanagedProjectNodes !== null && (
          <>
            <Button variant="outlined" onClick={downloadCsv}>
              Download CSV
            </Button>
            {navigator.clipboard && (
              <Button variant="outlined" onClick={copyMarkdownTable}>
                Copy Markdown Table
              </Button>
            )}
          </>
        )}
        {searchState !== "" && <Box>{searchState}</Box>}
      </Box>
      {!isSearching && unmanagedProjectNodes !== null && (
        <>
          <Typography variant="body1" sx={{ marginBottom: 2 }}>
            Unmanaged project count: {`${unmanagedProjectNodes.length}`} out of{" "}
            {`${totalProjectCount}`} or approximately{" "}
            {`${((100 * unmanagedProjectNodes.length) / totalProjectCount).toFixed(1)}%`}
            .
          </Typography>
          <TableContainer component={Paper} sx={{ marginBottom: 2 }}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Full Path</TableCell>
                  <TableCell>Name</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {unmanagedProjectNodes.map(({ fullPath, id, name, webUrl }) => (
                  <TableRow
                    key={id}
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      <Link href={`${webUrl}`}>
                        {fullPath}
                      </Link>
                    </TableCell>
                    <TableCell component="th" scope="row">
                      {name}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </>
      )}
    </Page>
  );
};

export default UnmanagedProjects;
