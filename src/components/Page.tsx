import { ReactNode } from "react";

import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Divider from "@mui/material/Divider";
import Drawer from "@mui/material/Drawer";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";

import { useTheme } from "@mui/material/styles";

import NavPane from "./NavPane";

const drawerWidth = 240;

export interface PageProps {
  title?: ReactNode;
  children: ReactNode;
}

export const Page = ({ title, children }: PageProps) => {
  const theme = useTheme();
  return (
    <>
      <Box sx={{ display: "flex" }}>
        <Drawer
          sx={{
            width: drawerWidth,
            flexShrink: 0,
            "& .MuiDrawer-paper": {
              width: drawerWidth,
              boxSizing: "border-box",
            },
          }}
          variant="permanent"
          anchor="left"
        >
          <Toolbar />
          <Divider />
          <NavPane />
        </Drawer>
        <Box
          component="main"
          sx={{ flexGrow: 1, bgcolor: "background.default" }}
        >
          <AppBar position="sticky">
            <Toolbar>
              <Typography
                variant="h6"
                component="div"
                sx={{
                  maxWidth: theme.spacing(100),
                  flexGrow: 1,
                  marginLeft: "auto",
                  marginRight: "auto",
                }}
              >
                {title ?? "GitLab Utilities"}
              </Typography>
            </Toolbar>
          </AppBar>

          <Box
            component="main"
            sx={{
              width: "100%",
              maxWidth: theme.spacing(100),
              marginLeft: "auto",
              marginRight: "auto",
              marginTop: theme.spacing(2),
            }}
          >
            {children}
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default Page;
