import { ReactNode } from "react";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Modal from "@mui/material/Modal";

import useGitLabAuth from "../hooks/useGitLabAuth";

const style = {
  position: "absolute" as "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

export const LoginRequired = ({ children }: { children: ReactNode }) => {
  const { loginInProgress, error } = useGitLabAuth();

  return (
    <>
      <Modal
        open={!!error}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography id="modal-modal-title" variant="h6" component="h2">
            There was an error.
          </Typography>
          <Typography id="modal-modal-description" sx={{ mt: 2 }}>
            There was an error signing you in: {error}
          </Typography>
        </Box>
      </Modal>
      {!loginInProgress && !error && children}
    </>
  );
};

export default LoginRequired;
