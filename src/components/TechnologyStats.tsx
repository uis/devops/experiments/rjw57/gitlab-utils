import React from "react";

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Typography from "@mui/material/Typography";
import MenuItem from "@mui/material/MenuItem";
import InputLabel from "@mui/material/InputLabel";

import { BarChart } from "@mui/x-charts/BarChart";
import { axisClasses } from "@mui/x-charts/ChartsAxis";

import { unparse as csvUnparse } from "papaparse";

import Page from "./Page";
import SelectGroup from "./SelectGroup";

import useGitLabAuth from "../hooks/useGitLabAuth";
import { fetchTechnologies, ProjectRecord } from "../queries/technologies";

const MAX_LANGUAGE_COUNT = 20;
const MAX_DEPENDENCY_COUNT = 20;

const downloadCsvBlob = (csvContents: string, filename: string) => {
  const csvBlob = new Blob([csvContents], {
    type: "text/csv;charset=utf-8;",
  });
  var csvUrl = URL.createObjectURL(csvBlob);
  var downloadAnchor = document.createElement("a");
  downloadAnchor.href = csvUrl;
  downloadAnchor.setAttribute("download", filename);
  downloadAnchor.click();
};

const makeLanguagesCsv = (
  languageCounts: { language: string; value: number }[],
) => {
  const rows = languageCounts.map(({ language, value }) => [language, value]);
  rows.unshift(["Language", "Count"]);
  return csvUnparse(rows);
};

const makeDependenciesCsv = (dependencyCounts: {
  [packager: string]: { name: string; value: number }[];
}) => {
  const rows = [["Packager", "Dependency", "Count"]];
  Object.entries(dependencyCounts).forEach(([packager, counts]) => {
    counts.forEach(({ name, value }) => {
      rows.push([packager, name, `${value}`]);
    });
  });
  return csvUnparse(rows);
};

export const TechnologyStats = () => {
  const [isLoading, setIsLoading] = React.useState(false);
  const [latestMessage, setLatestMessage] = React.useState("");
  const [allProjects, setAllProjects] = React.useState<ProjectRecord[] | null>(
    null,
  );
  const [groupFullPath, setGroupFullPath] = React.useState<string>("");
  const [packager, setPackager] = React.useState("");
  const { token } = useGitLabAuth();

  const formDisabled = (groupFullPath && groupFullPath !== "") || isLoading;
  const submitDisabled = !groupFullPath || groupFullPath === "" || isLoading;

  const startSearch = async () => {
    setIsLoading(true);
    setAllProjects(null);
    try {
      const allProjects = await fetchTechnologies(token, groupFullPath, {
        logMessage: setLatestMessage,
      });
      setAllProjects(allProjects);
    } finally {
      setIsLoading(false);
    }
  };

  const languageCounts = React.useMemo(() => {
    const counts: { [key: string]: number } = {};
    allProjects?.forEach(({ languages }) => {
      languages.forEach((language) => {
        counts[language] = (counts[language] ?? 0) + 1;
      });
    });
    const sortedCounts = Object.entries(counts)
      .map(([language, value]) => ({
        language,
        value,
      }))
      .sort(
        ({ language: aLabel, value: a }, { language: bLabel, value: b }) => {
          if (a !== b) {
            return b - a;
          }
          return aLabel.localeCompare(bLabel);
        },
      );

    return sortedCounts;
  }, [allProjects]);

  const dependencyCounts = React.useMemo(() => {
    const countsByPackager: { [packager: string]: { [dep: string]: number } } =
      {};
    allProjects?.forEach(({ dependencies }) => {
      dependencies.forEach(({ packager, name }) => {
        const packagerCounts = countsByPackager[packager] ?? {};
        packagerCounts[name] = (packagerCounts[name] ?? 0) + 1;
        countsByPackager[packager] = packagerCounts;
      });
    });

    const sortedCountsByPackager: {
      [packager: string]: { name: string; value: number }[];
    } = {};
    Object.entries(countsByPackager).forEach(([packager, counts]) => {
      const sortedCounts = Object.entries(counts)
        .map(([name, value]) => ({
          name,
          value,
        }))
        .sort(({ name: aLabel, value: a }, { name: bLabel, value: b }) => {
          if (a !== b) {
            return b - a;
          }
          return aLabel.localeCompare(bLabel);
        });
      sortedCountsByPackager[packager] = sortedCounts;
    });

    setPackager(Object.keys(sortedCountsByPackager).sort()[0] ?? "");

    return sortedCountsByPackager;
  }, [allProjects, setPackager]);

  const downloadLanguagesCsv = () => {
    downloadCsvBlob(makeLanguagesCsv(languageCounts), "language-counts.csv");
  };

  const downloadDependenciesCsv = () => {
    downloadCsvBlob(
      makeDependenciesCsv(dependencyCounts),
      "dependency-counts.csv",
    );
  };

  return (
    <Page title="Technology usage statistics">
      <Typography variant="body1" sx={{ marginBottom: 2 }}>
        Shows usage statistics for languages and dependencies accross projects.
      </Typography>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          startSearch();
        }}
      >
        <Box
          display="flex"
          gap={2}
          flexDirection="column"
          sx={{ marginBottom: 2 }}
        >
          <SelectGroup
            TextFieldProps={{ label: "Parent group", disabled: formDisabled }}
            onChange={(_, fullPath) => {
              setGroupFullPath(fullPath);
            }}
          />
          <Button type="submit" disabled={submitDisabled} variant="contained">
            Analyse
          </Button>
        </Box>
      </form>
      {isLoading && (
        <>
          <Typography variant="body1" marginBottom={1}>
            Loading&hellip; This can take some time for large groups.
          </Typography>
          <Typography variant="body2">{latestMessage}</Typography>
        </>
      )}
      {allProjects && (
        <Box>
          <Box flexDirection="row" display="flex" alignItems="baseline">
            <Typography variant="h6" flexGrow={1}>
              Project counts by language
            </Typography>
            <Button variant="outlined" onClick={downloadLanguagesCsv}>
              Download CSV
            </Button>
          </Box>
          <BarChart
            sx={{
              [`.${axisClasses.left} .${axisClasses.label}`]: {
                // Move the y-axis label with CSS
                transform: "translateX(-20px)",
              },
            }}
            xAxis={[
              {
                label: "Language",
                scaleType: "band",
                data: languageCounts
                  .slice(0, MAX_LANGUAGE_COUNT)
                  .map(({ language }) => language),
              },
            ]}
            yAxis={[{ label: "Project count" }]}
            series={[
              {
                data: languageCounts
                  .slice(0, MAX_LANGUAGE_COUNT)
                  .map(({ value }) => value),
              },
            ]}
            height={400}
            leftAxis={{
              labelStyle: {
                fontSize: 14,
              },
            }}
            margin={{
              left: 80,
              bottom: 120,
            }}
            bottomAxis={{
              tickLabelInterval: () => true,
              labelStyle: {
                fontSize: 14,
                transform: "translateY(60px)",
              },
              tickLabelStyle: {
                angle: 60,
                textAnchor: "start",
                fontSize: 10,
              },
            }}
          />
          <Box
            flexDirection="row"
            display="flex"
            alignItems="baseline"
            marginBottom={2}
          >
            <Typography variant="h6" flexGrow={1}>
              Project counts by dependency
            </Typography>
            <Button variant="outlined" onClick={downloadDependenciesCsv}>
              Download CSV
            </Button>
          </Box>
          <FormControl fullWidth>
            <InputLabel id="packager-label">Packager</InputLabel>
            <Select
              labelId="packager-label"
              value={packager}
              label="Packager"
              onChange={(e) => {
                setPackager(e.target.value);
              }}
            >
              {Object.keys(dependencyCounts)
                .sort()
                .map((packager) => (
                  <MenuItem value={packager}>{packager}</MenuItem>
                ))}
            </Select>
          </FormControl>
          <BarChart
            sx={{
              [`.${axisClasses.left} .${axisClasses.label}`]: {
                // Move the y-axis label with CSS
                transform: "translateX(-20px)",
              },
            }}
            xAxis={[
              {
                label: "Dependency",
                scaleType: "band",
                data: (dependencyCounts[packager] ?? [])
                  .slice(0, MAX_DEPENDENCY_COUNT)
                  .map(({ name }) => name),
              },
            ]}
            yAxis={[{ label: "Project count" }]}
            series={[
              {
                data: (dependencyCounts[packager] ?? [])
                  .slice(0, MAX_DEPENDENCY_COUNT)
                  .map(({ value }) => value),
              },
            ]}
            height={400}
            leftAxis={{
              labelStyle: {
                fontSize: 14,
              },
            }}
            margin={{
              left: 80,
              bottom: 120,
            }}
            bottomAxis={{
              tickLabelInterval: () => true,
              labelStyle: {
                fontSize: 14,
                transform: "translateY(60px)",
              },
              tickLabelStyle: {
                angle: 60,
                textAnchor: "start",
                fontSize: 10,
              },
            }}
          />
        </Box>
      )}
    </Page>
  );
};

export default TechnologyStats;
