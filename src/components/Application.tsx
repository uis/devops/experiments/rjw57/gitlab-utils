import { useMemo } from "react";
import useMediaQuery from "@mui/material/useMediaQuery";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import {
  QueryClient,
  QueryClientProvider,
  QueryCache,
  MutationCache,
} from "@tanstack/react-query";
import { createHashRouter, RouterProvider } from "react-router-dom";
import CssBaseline from "@mui/material/CssBaseline";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import GitLabAuthProvider from "./GitLabAuthProvider";
import LoginRequired from "./LoginRequired";

import IndexPage from "./IndexPage";
import ApprovalRules from "./ApprovalRules";
import Dependencies from "./Dependencies";
import UnmanagedProjects from "./UnmanagedProjects";
import GenerateFactoryConfig from "./GenerateFactoryConfig";
import TechnologyStats from "./TechnologyStats";

const router = createHashRouter([
  {
    path: "/",
    element: <IndexPage />,
  },
  {
    path: "/approval-rules",
    element: <ApprovalRules />,
  },
  {
    path: "/dependencies",
    element: <Dependencies />,
  },
  {
    path: "/unmanaged-projects",
    element: <UnmanagedProjects />,
  },
  {
    path: "/generate-factory-config",
    element: <GenerateFactoryConfig />,
  },
  {
    path: "/technology-stats",
    element: <TechnologyStats />,
  },
]);

export const Application = () => {
  const prefersDarkMode = useMediaQuery("(prefers-color-scheme: dark)");

  const queryClient = useMemo(
    () =>
      new QueryClient({
        queryCache: new QueryCache({
          onError: (error) => {
            toast.error("Something went wrong, see the developer console.");
            console.log(error);
          },
        }),
        mutationCache: new MutationCache({
          onError: (error) => {
            toast.error("Something went wrong, see the developer console.");
            console.log(error);
          },
        }),
      }),
    [],
  );
  const theme = useMemo(
    () =>
      createTheme({
        palette: {
          mode: prefersDarkMode ? "dark" : "light",
        },
      }),
    [prefersDarkMode],
  );

  return (
    <QueryClientProvider client={queryClient}>
      <ThemeProvider theme={theme}>
        <CssBaseline />
        <GitLabAuthProvider>
          <LoginRequired>
            <RouterProvider router={router} />
            <ToastContainer />
          </LoginRequired>
        </GitLabAuthProvider>
      </ThemeProvider>
    </QueryClientProvider>
  );
};

export default Application;
