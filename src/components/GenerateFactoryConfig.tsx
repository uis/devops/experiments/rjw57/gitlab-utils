import { useState, useMemo, useEffect } from "react";

import Accordion from "@mui/material/Accordion";
import AccordionDetails from "@mui/material/AccordionDetails";
import AccordionSummary from "@mui/material/AccordionSummary";
import Box from "@mui/material/Box";
import Checkbox from "@mui/material/Checkbox";
import FormControlLabel from "@mui/material/FormControlLabel";
import FormGroup from "@mui/material/FormGroup";
import IconButton from "@mui/material/IconButton";
import InputAdornment from "@mui/material/InputAdornment";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";

import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";

import { gql } from "graphql-request";

import { queue } from "async";

import { PROJECT_FACTORY_TOPIC, GITLAB_BASE_URL } from "../config";
import Page from "./Page";
import SelectGroup from "./SelectGroup";
import usePaginatedGraphQLQuery from "../hooks/usePaginatedGraphQLQuery";
import useGitLabAuth from "../hooks/useGitLabAuth";

const PROJECT_FETCH_CONCURRENCY = 5;

interface ProjectNode {
  id: string;
  path: string;
  fullPath: string;
  name?: string;
  description?: string;
  topics: string[];
  visibility: string;
  webUrl: string;

  branchRules: {
    nodes: {
      id: string;
      name: string;
      approvalRules: {
        nodes: {
          name: string;
          id: string;
        }[];
      };
    }[];
  };

  repository: {
    rootRef: string;
  } | null;
}

interface GroupNode {
  id: string;
  name: string | null;
  path: string;
  visibility: string;
  description: string | null;
  projects: {
    nodes: ProjectNode[];
    pageInfo: {
      endCursor: string;
      hasNextPage: boolean;
    };
  };
}

interface GroupProjectsQueryData {
  group: GroupNode;
}

const groupProjectsQuery = gql`
  query groupProjects($namespaceFullPath: ID!, $after: String) {
    group(fullPath: $namespaceFullPath) {
      id
      name
      path
      visibility
      description
      projects(after: $after, includeArchived: false) {
        nodes {
          id
          path
          fullPath
          name
          description
          topics
          visibility
          webUrl

          branchRules {
            nodes {
              id
              name
              approvalRules {
                nodes {
                  name
                  id
                }
              }
            }
          }

          repository {
            rootRef
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`;

interface ProjectResource {
  default_branch: string;
  description: string | null;
  display_name: string | null;
  issues_template: string | null;
  issues_template_filename: string | null;
  import_url: string | null;

  analytics_access_level: string;
  builds_access_level: string;
  container_registry_access_level: string;
  environments_access_level: string;
  feature_flags_access_level: string;
  forking_access_level: string;
  infrastructure_access_level: string;
  issues_access_level: string;
  merge_requests_access_level: string;
  monitor_access_level: string;
  pages_access_level: string;
  releases_access_level: string;
  repository_access_level: string;
  requirements_access_level: string;
  security_and_compliance_access_level: string;
  snippets_access_level: string;
  wiki_access_level: string;
}

const PROJECT_DEFAULTS = {
  default_branch: "main",
  description: null,
  display_name: null,
  issues_template: null,
  issues_template_filename: null,
  import_url: null,

  analytics_access_level: "enabled",
  builds_access_level: "enabled",
  container_registry_access_level: "enabled",
  environments_access_level: "enabled",
  feature_flags_access_level: "enabled",
  forking_access_level: "enabled",
  infrastructure_access_level: "enabled",
  issues_access_level: "enabled",
  merge_requests_access_level: "enabled",
  monitor_access_level: "enabled",
  pages_access_level: "private",
  releases_access_level: "enabled",
  repository_access_level: "enabled",
  requirements_access_level: "enabled",
  security_and_compliance_access_level: "private",
  snippets_access_level: "enabled",
  wiki_access_level: "enabled",
};

const ACCESS_LEVEL_KEYS: (keyof ProjectResource)[] = [
  "analytics_access_level",
  "builds_access_level",
  "container_registry_access_level",
  "environments_access_level",
  "feature_flags_access_level",
  "forking_access_level",
  "infrastructure_access_level",
  "issues_access_level",
  "merge_requests_access_level",
  "monitor_access_level",
  "pages_access_level",
  "releases_access_level",
  "repository_access_level",
  "requirements_access_level",
  "security_and_compliance_access_level",
  "snippets_access_level",
  "wiki_access_level",
];

const hclString = (v?: string) => (v ? JSON.stringify(v) : "null");
const hclStringList = (v?: string[]) =>
  v ? `[${v.map(hclString).join(", ")}]` : "null";

const makeTerraformImport = (
  projects: ProjectNode[],
  product_vars: string,
  group: GroupNode,
  isSubGroup: boolean,
  projectKeyPrefix: string,
): string => {
  const statements: string[] = [];
  const prefix = `./run-project-factory.sh -p product-vars/${product_vars} \\\n  `;

  if (isSubGroup) {
    const groupId = group.id.split("/").slice(-1)[0];
    statements.push(
      `${prefix} import 'gitlab_group.subgroup[${hclString(group.path)}]' ${groupId}`,
    );
  }

  projects.forEach((project) => {
    const projectId = project.id.split("/").slice(-1)[0];
    const projectKey = `${projectKeyPrefix}${project.path}`;
    statements.push(
      `${prefix} import 'module.project[${hclString(projectKey)}].gitlab_project.main' ${projectId}`,
    );

    const defaultBranch = project.repository?.rootRef ?? "main";
    project.branchRules.nodes.forEach(
      ({ id, name, approvalRules: { nodes: approvalRuleNodes } }) => {
        if (name === defaultBranch) {
          statements.push(
            `${prefix} import 'module.project[${hclString(projectKey)}].gitlab_branch_protection.default[0]' ${projectId}:${defaultBranch}`,
          );
        }
        approvalRuleNodes.forEach(({ id, name }) => {
          const ruleId = id.split("/").slice(-1)[0];
          if (name === "All Members") {
            statements.push(
              `${prefix} import 'module.project[${hclString(projectKey)}].gitlab_project_approval_rule.default' ${projectId}:${ruleId}`,
            );
          }
        });
      },
    );
  });

  return statements.join("\n");
};

const PROJECT_CACHE = new Map<string, ProjectResource>();

const addStringStatement = (
  statements: [string, string][],
  k: string,
  v?: string,
  options?: {
    allowMultiLine: boolean;
  },
) => {
  const { allowMultiLine } = { allowMultiLine: false, ...options };
  if (allowMultiLine && v && v.length > 50) {
    statements.push([
      k,
      `<<-EOI\n${v
        .trimEnd()
        .split("\n")
        .map((s) => (s.trim() !== "" ? `      ${s}` : ""))
        .join("\n")}\n    EOI`,
    ]);
  } else {
    statements.push([k, hclString(v)]);
  }
};

const addStringListStatement = (
  statements: [string, string][],
  k: string,
  v?: string[],
) => {
  statements.push([k, hclStringList(v)]);
};

const makeStatementsString = (
  statements: [string, string][],
  leftPad: string = "",
) => {
  const maxLen = Math.max(...statements.map(([lhs, _]) => lhs.length));
  return statements
    .map(([lhs, rhs]) => `${leftPad}${lhs.padEnd(maxLen, " ")} = ${rhs}\n`)
    .join("");
};

const makeProductVars = (
  projects: ProjectNode[],
  group: GroupNode,
  isSubgroup: boolean,
  projectKeyPrefix: string,
): string => {
  const processProject = (project: ProjectNode) => {
    const filteredTopics = project.topics.filter(
      (t) => t !== PROJECT_FACTORY_TOPIC,
    );
    const statements: [string, string][] = [];
    const projectResource = PROJECT_CACHE.get(project.id);

    if (projectKeyPrefix !== "") {
      addStringStatement(statements, "path", project.path);
    }

    if (
      project.repository &&
      project.repository.rootRef !== PROJECT_DEFAULTS.default_branch
    ) {
      addStringStatement(
        statements,
        "default_branch",
        project.repository.rootRef,
      );
    }
    if (project.description) {
      addStringStatement(statements, "description", project.description, {
        allowMultiLine: true,
      });
    }
    addStringStatement(statements, "display_name", project.name);
    if (isSubgroup) {
      addStringStatement(statements, "subgroup", group.path);
    }
    if (project.visibility !== "public") {
      addStringStatement(statements, "visibility_level", project.visibility);
    }
    if (filteredTopics.length > 0) {
      addStringListStatement(statements, "topics", filteredTopics);
    }

    if (projectResource) {
      if (projectResource.issues_template) {
        addStringStatement(
          statements,
          "issues_template",
          projectResource.issues_template,
          { allowMultiLine: true },
        );
      }

      if (projectResource.import_url) {
        addStringStatement(
          statements,
          "import_url",
          projectResource.import_url,
        );
      }

      ACCESS_LEVEL_KEYS.forEach((key) => {
        if (
          projectResource[key] &&
          projectResource[key] !== PROJECT_DEFAULTS[key]
        ) {
          addStringStatement(statements, key, projectResource[key]);
        }
      });
    }

    return [
      `  ${hclString(`${projectKeyPrefix}${project.path}`)} = {\n`,
      makeStatementsString(statements, "    "),
      `  }`,
    ].join("");
  };

  const stanzas: string[] = [];

  if (isSubgroup) {
    const statements: [string, string][] = [];
    if (group.name) {
      addStringStatement(statements, "display_name", group.name);
    }
    if (group.description) {
      addStringStatement(statements, "description", group.description, {
        allowMultiLine: true,
      });
    }
    addStringStatement(statements, "visibility_level", group.visibility);
    stanzas.push(
      `subgroups = {\n  ${hclString(group.path)} = {\n${makeStatementsString(statements, "    ")}  }\n}`,
    );
  }
  stanzas.push(`projects = {\n${projects.map(processProject).join("\n\n")}\n}`);

  return stanzas.join("\n\n");
};

export const GenerateFactoryConfig = () => {
  const { token } = useGitLabAuth();
  const [isFetchingProjects, setIsFetchingProjects] = useState(false);
  const [includeAlreadyManaged, setIncludeAlreadyManaged] = useState(false);
  const [namespaceFullPath, setNamespaceFullPath] = useState("");
  const [productVarsDir, setProductVarsDir] = useState("");
  const [isSubgroup, setIsSubgroup] = useState(false);
  const [projectKeyPrefix, setProjectKeyPrefix] = useState("");
  const { data: groupData } = usePaginatedGraphQLQuery<GroupProjectsQueryData>(
    groupProjectsQuery,
    {
      namespaceFullPath,
    },
    {
      enabled: namespaceFullPath !== "",
      nextPageVariablesFn: (data: GroupProjectsQueryData) => {
        if (!data.group.projects.pageInfo.hasNextPage) {
          return null;
        }
        return { after: data.group.projects.pageInfo.endCursor };
      },
    },
  );
  const projectNodes: ProjectNode[] | null = useMemo(
    () =>
      groupData
        ? new Array<ProjectNode>()
            .concat(...groupData.map((d) => d.group.projects.nodes))
            .filter(
              ({ topics }) =>
                includeAlreadyManaged ||
                !topics.includes(PROJECT_FACTORY_TOPIC),
            )
            .toSorted((a, b) => a.path.localeCompare(b.path))
        : null,
    [groupData, includeAlreadyManaged],
  );

  const projectFetchQueue = useMemo(
    () =>
      queue(async (projectId: string) => {
        if (PROJECT_CACHE.has(projectId)) {
          return;
        }

        const projectRestId = projectId.split("/").slice(-1)[0];
        const response = await fetch(
          `${GITLAB_BASE_URL}api/v4/projects/${projectRestId}`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          },
        );
        if (!response.ok) {
          throw new Error("Error fetching GitLab project");
        }
        PROJECT_CACHE.set(projectId, await response.json());
      }, PROJECT_FETCH_CONCURRENCY),
    [token],
  );

  useEffect(() => {
    if (!projectNodes || projectNodes.length === 0) {
      return;
    }
    setIsFetchingProjects(true);
    projectNodes.forEach(({ id }) => {
      projectFetchQueue.push(id);
    });
    projectFetchQueue.drain(() => {
      setIsFetchingProjects(false);
    });
  }, [projectFetchQueue, projectNodes]);

  const groupNode = groupData && groupData[0].group;
  const productVars = useMemo(
    () =>
      projectNodes && !isFetchingProjects && groupNode
        ? makeProductVars(projectNodes, groupNode, isSubgroup, projectKeyPrefix)
        : "",
    [projectNodes, isFetchingProjects, isSubgroup, groupNode, projectKeyPrefix],
  );
  const terraformImport = useMemo(
    () =>
      projectNodes && !isFetchingProjects && groupNode && productVarsDir !== ""
        ? makeTerraformImport(
            projectNodes,
            productVarsDir,
            groupNode,
            isSubgroup,
            projectKeyPrefix,
          )
        : "",
    [
      projectNodes,
      productVarsDir,
      isFetchingProjects,
      isSubgroup,
      groupNode,
      projectKeyPrefix,
    ],
  );

  return (
    <Page title="Generate project factory configuration">
      <Typography variant="body1" sx={{ marginBottom: 2 }}>
        Generate configuration for <code>product-vars</code> for the GitLab
        project factory.
      </Typography>
      <Box sx={{ marginBottom: 2 }}>
        <Box
          component={FormGroup}
          sx={{ display: "flex" }}
          gap={2}
          flexDirection="column"
        >
          <SelectGroup
            TextFieldProps={{ label: "Select group to generate config" }}
            onChange={(_, fullPath) => {
              setNamespaceFullPath(fullPath);
            }}
          />
          <TextField
            label="Product vars directory"
            value={productVarsDir}
            onChange={(e) => {
              setProductVarsDir(e.target.value);
            }}
            slotProps={{
              input: {
                startAdornment: (
                  <InputAdornment position="start">
                    product-vars/
                  </InputAdornment>
                ),
              },
            }}
          />
          <TextField
            label="Project key prefix"
            helperText="Useful for projects within subgroups which share names"
            value={projectKeyPrefix}
            onChange={(e) => {
              setProjectKeyPrefix(e.target.value);
            }}
          />
          <FormControlLabel
            control={
              <Checkbox
                checked={isSubgroup}
                onChange={(e) => {
                  setIsSubgroup(e.target.checked);
                }}
              />
            }
            label="Is subgroup of product group"
          />
          <FormControlLabel
            control={
              <Checkbox
                checked={includeAlreadyManaged}
                onChange={(e) => {
                  setIncludeAlreadyManaged(e.target.checked);
                }}
              />
            }
            label="Include already managed projects"
          />
        </Box>
      </Box>
      {groupData && (
        <Box>
          <Accordion defaultExpanded>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              <Box
                sx={{ display: "flex", width: "100%", alignItems: "baseline" }}
              >
                <Box sx={{ flexGrow: 1 }}>Product Vars</Box>
                {navigator.clipboard && (
                  <IconButton
                    size="small"
                    onClick={(e) => {
                      e.stopPropagation();
                      navigator.clipboard.writeText(productVars);
                    }}
                  >
                    <ContentCopyIcon fontSize="inherit" />
                  </IconButton>
                )}
              </Box>
            </AccordionSummary>
            <AccordionDetails>
              <code>
                <Box component="pre" sx={{ margin: 0 }}>
                  {productVars}
                </Box>
              </code>
            </AccordionDetails>
          </Accordion>
          <Accordion defaultExpanded>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
              <Box
                sx={{ display: "flex", width: "100%", alignItems: "baseline" }}
              >
                <Box sx={{ flexGrow: 1 }}>Import commands</Box>
                {navigator.clipboard && (
                  <IconButton
                    size="small"
                    onClick={(e) => {
                      e.stopPropagation();
                      navigator.clipboard.writeText(terraformImport);
                    }}
                  >
                    <ContentCopyIcon fontSize="inherit" />
                  </IconButton>
                )}
              </Box>
            </AccordionSummary>
            <AccordionDetails>
              <code>
                <Box component="pre" sx={{ margin: 0 }}>
                  {terraformImport}
                </Box>
              </code>
            </AccordionDetails>
          </Accordion>
        </Box>
      )}
    </Page>
  );
};

export default GenerateFactoryConfig;
