import { useState, useMemo, ComponentProps, SyntheticEvent } from "react";
import Autocomplete from "@mui/material/Autocomplete";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import { gql } from "graphql-request";
import { useDebounce } from "@uidotdev/usehooks";

import useGraphQLQuery from "../hooks/useGraphQLQuery";

export interface GroupSearchQueryNode {
  id: string;
  fullPath: string;
  description: string;
  name: string;
  path: string;
}

interface GroupSearchQueryData {
  groups: {
    nodes: GroupSearchQueryNode[];
  };
}

const groupSearchQuery = gql`
  query groupSearch($search: String!) {
    groups(search: $search) {
      nodes {
        id
        fullPath
        description
        name
        path
      }
    }
  }
`;

export interface SelectGroupProps {
  onChange?: (
    event: SyntheticEvent,
    value: string,
    groupNode: GroupSearchQueryNode | null,
  ) => void;
  TextFieldProps?: ComponentProps<typeof TextField>;
}

export const SelectGroup = ({ onChange, TextFieldProps }: SelectGroupProps) => {
  const [value, setValue] = useState<GroupSearchQueryNode | null>(null);
  const [search, setSearch] = useState<string>();
  const debouncedSearch = useDebounce(search, 300);
  const { data, isLoading } = useGraphQLQuery<GroupSearchQueryData>(
    groupSearchQuery,
    {
      search: debouncedSearch,
    },
    {
      enabled: !!debouncedSearch && debouncedSearch !== "",
    },
  );

  // Make sure options are sorted by fullPath..
  const options = useMemo(() => {
    if (!data) {
      return [];
    }
    const options = [...data.groups.nodes];
    options.sort((a, b) => a.fullPath.localeCompare(b.fullPath));
    return options;
  }, [data]);

  return (
    <Autocomplete
      loading={isLoading}
      options={options}
      value={value}
      onChange={(e, newValue: GroupSearchQueryNode | null) => {
        setValue(newValue);
        onChange && onChange(e, newValue?.fullPath ?? "", newValue);
      }}
      filterOptions={(options) => options}
      isOptionEqualToValue={(option, value) => option.id === value.id}
      getOptionLabel={({ fullPath }) => fullPath}
      getOptionKey={({ id }) => id}
      onInputChange={(_: any, newValue: string) => {
        setSearch(newValue);
      }}
      renderOption={(props, option) => (
        <li {...props}>
          <Box>
            <Box>{option.name}</Box>
            <Typography variant="body2" color="text.secondary">
              {option.fullPath}
            </Typography>
          </Box>
        </li>
      )}
      renderInput={(params) => (
        <TextField
          label="GitLab Group"
          type="search"
          {...TextFieldProps}
          {...params}
          InputProps={{
            ...params.InputProps,
            ...TextFieldProps?.InputProps,
            endAdornment: (
              <>
                {isLoading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
                {TextFieldProps?.InputProps?.endAdornment}
              </>
            ),
          }}
        />
      )}
    />
  );
};

export default SelectGroup;
