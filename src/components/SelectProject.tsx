import { useState, useMemo, ComponentProps, SyntheticEvent } from "react";
import Autocomplete from "@mui/material/Autocomplete";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";
import { gql } from "graphql-request";
import { useDebounce } from "@uidotdev/usehooks";

import useGraphQLQuery from "../hooks/useGraphQLQuery";

export interface ProjectSearchQueryNode {
  id: string;
  fullPath: string;
  description: string;
  name: string;
  path: string;
  namespace?: {
    fullPath: string;
  };
}

interface ProjectSearchQueryData {
  projects: {
    nodes: ProjectSearchQueryNode[];
  };
}

const projectSearchQuery = gql`
  query projectSearch($search: String!) {
    projects(search: $search, searchNamespaces: true) {
      nodes {
        id
        fullPath
        description
        name
        path
        namespace {
          fullPath
        }
      }
    }
  }
`;

export interface SelectProjectProps {
  onChange?: (event: SyntheticEvent, value: string, projectNode: ProjectSearchQueryNode | null) => void;
  TextFieldProps?: ComponentProps<typeof TextField>;
}

export const SelectProject = ({
  onChange,
  TextFieldProps,
}: SelectProjectProps) => {
  const [value, setValue] = useState<ProjectSearchQueryNode | null>(null);
  const [search, setSearch] = useState<string>();
  const debouncedSearch = useDebounce(search, 300);
  const { data, isLoading } = useGraphQLQuery<ProjectSearchQueryData>(
    projectSearchQuery,
    {
      search: debouncedSearch,
    },
    {
      enabled: !!debouncedSearch && debouncedSearch !== "",
    },
  );

  // Make sure options are sorted by namespace.
  const options = useMemo(() => {
    if (!data) {
      return [];
    }
    const options = [...data.projects.nodes];
    options.sort((a, b) =>
      (a.namespace?.fullPath ?? a.fullPath).localeCompare(
        b.namespace?.fullPath ?? b.fullPath,
      ),
    );
    return options;
  }, [data]);

  return (
    <Autocomplete
      loading={isLoading}
      options={options}
      value={value}
      onChange={(e, newValue: ProjectSearchQueryNode | null) => {
        setValue(newValue);
        onChange && onChange(e, newValue?.fullPath ?? "", newValue);
      }}
      filterOptions={(options) => options}
      isOptionEqualToValue={(option, value) => option.id === value.id}
      groupBy={({ namespace, fullPath }) =>
        namespace?.fullPath ?? fullPath.split("/").slice(0, -1).join("/")
      }
      getOptionLabel={({ fullPath }) => fullPath}
      getOptionKey={({ id }) => id}
      onInputChange={(_: any, newValue: string) => {
        setSearch(newValue);
      }}
      renderOption={(props, option) => (
        <li {...props}>
          <Box>
            <Box>{option.name}</Box>
            {option.description && option.description !== "" && (
              <Typography variant="body2" color="text.secondary">
                {option.description}
              </Typography>
            )}
          </Box>
        </li>
      )}
      renderInput={(params) => (
        <TextField
          label="GitLab Project"
          type="search"
          {...TextFieldProps}
          {...params}
          InputProps={{
            ...params.InputProps,
            ...TextFieldProps?.InputProps,
            endAdornment: (
              <>
                {isLoading ? (
                  <CircularProgress color="inherit" size={20} />
                ) : null}
                {params.InputProps.endAdornment}
                {TextFieldProps?.InputProps?.endAdornment}
              </>
            ),
          }}
        />
      )}
    />
  );
};

export default SelectProject;
