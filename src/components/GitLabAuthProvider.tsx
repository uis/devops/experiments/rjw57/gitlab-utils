import { ReactNode } from "react";
import {
  AuthProvider,
  TAuthConfig,
  TRefreshTokenExpiredEvent,
} from "react-oauth2-code-pkce";

const authConfig: TAuthConfig = {
  clientId: "3a853e74549e9e11c15b98b21bff5cc37e0466e8b59c7c62b5424c101d818ce5",
  loginMethod: "redirect",
  authorizationEndpoint: "https://gitlab.developers.cam.ac.uk/oauth/authorize",
  tokenEndpoint: "https://gitlab.developers.cam.ac.uk/oauth/token",
  redirectUri: window.location.origin,
  scope: "read_api profile email openid",
  decodeToken: false,
  onRefreshTokenExpire: (event: TRefreshTokenExpiredEvent) =>
    window.confirm(
      "Session expired. Refresh page to continue using the site?",
    ) && event.login(),
};

export const GitLabAuthProvider = ({ children }: { children: ReactNode }) => {
  return <AuthProvider authConfig={authConfig}>{children}</AuthProvider>;
};

export default GitLabAuthProvider;
