import { useState, useMemo } from "react";

import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Link from "@mui/material/Link";
import Paper from "@mui/material/Paper";
import Skeleton from "@mui/material/Skeleton";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import TextField from "@mui/material/TextField";
import Typography from "@mui/material/Typography";

import { gql } from "graphql-request";
import useInfiniteScroll from "react-infinite-scroll-hook";

import Page from "./Page";
import useGraphQLInfiniteQuery from "../hooks/useGraphQLInfiniteQuery";

import SelectGroup from "./SelectGroup";

interface DependenciesQueryData {
  projects: {
    nodes: {
      id: string;
      name: string;
      fullPath: string;
      dependencies?: {
        nodes: {
          id: string;
          location: { blobPath: string; path: string };
          name: string;
          packager: string;
          version: string;
        }[];
      };
    }[];
    pageInfo: {
      endCursor: string;
      hasNextPage: boolean;
    };
  };
}

const dependenciesQuery = gql`
  query dependencies($after: String, $componentNames: [String!]) {
    projects(after: $after) {
      nodes {
        id
        name
        fullPath
        dependencies(componentNames: $componentNames) {
          nodes {
            id
            location {
              blobPath
              path
            }
            name
            packager
            version
          }
        }
      }
      pageInfo {
        endCursor
        hasNextPage
      }
    }
  }
`;

interface NamespacedDependenciesQueryData {
  namespace: DependenciesQueryData;
}

const namespacedDependenciesQuery = gql`
  query namespacedDependencies(
    $after: String
    $componentNames: [String!]
    $namespaceFullPath: ID!
  ) {
    namespace(fullPath: $namespaceFullPath) {
      projects(includeSubgroups: true, after: $after) {
        nodes {
          id
          name
          fullPath
          dependencies(componentNames: $componentNames) {
            nodes {
              id
              location {
                blobPath
                path
              }
              name
              packager
              version
            }
          }
        }
        pageInfo {
          endCursor
          hasNextPage
        }
      }
    }
  }
`;

interface SearchQuery {
  namespaceFullPath: string;
  dependency: string;
}

export const Dependencies = () => {
  const [namespaceFullPath, setNamespaceFullPath] = useState<string>("");
  const [dependency, setDependency] = useState<string>("");
  const [search, setSearch] = useState<SearchQuery | null>(null);

  const nonNamespacedQuery = useGraphQLInfiniteQuery<DependenciesQueryData>(
    dependenciesQuery,
    ({
      projects: {
        pageInfo: { endCursor, hasNextPage },
      },
    }) => (hasNextPage ? endCursor : undefined),
    {
      componentNames: [search?.dependency],
    },
    {
      enabled: !!search && search.namespaceFullPath === "",
    },
  );

  const namespacedQuery =
    useGraphQLInfiniteQuery<NamespacedDependenciesQueryData>(
      namespacedDependenciesQuery,
      ({
        namespace: {
          projects: {
            pageInfo: { endCursor, hasNextPage },
          },
        },
      }) => (hasNextPage ? endCursor : undefined),
      {
        componentNames: [search?.dependency],
        namespaceFullPath: search?.namespaceFullPath,
      },
      {
        enabled: !!search && search.namespaceFullPath !== "",
      },
    );

  const { data, isLoading, isFetchingNextPage, hasNextPage, fetchNextPage } =
    namespaceFullPath === "" ? nonNamespacedQuery : namespacedQuery;

  const [sentryRef] = useInfiniteScroll({
    loading: isFetchingNextPage,
    hasNextPage,
    onLoadMore: () => {
      fetchNextPage && fetchNextPage();
    },
    disabled: !hasNextPage,
  });

  const showLoadingIndicator = isLoading || hasNextPage || isFetchingNextPage;

  const filteredProjects = useMemo(
    () =>
      data?.pages
        .map((page) =>
          Object.hasOwn(page, "namespace")
            ? (page as NamespacedDependenciesQueryData).namespace.projects
            : (page as DependenciesQueryData).projects,
        )
        .map(({ nodes }) =>
          nodes
            .filter(
              ({ dependencies }) =>
                !!dependencies && dependencies.nodes.length > 0,
            )
            .map(({ dependencies, ...project }) =>
              (dependencies?.nodes ?? []).map((dependency) => ({
                project,
                dependency: dependency,
              })),
            ),
        )
        .flat(2) ?? [],
    [data?.pages],
  );

  return (
    <Page title="Project dependencies">
      <Typography variant="body1" sx={{ marginBottom: 2 }}>
        Search for projects with a given named dependency.
      </Typography>
      <Box sx={{ marginBottom: 2 }}>
        <form
          onSubmit={(e) => {
            e.preventDefault();
            setSearch({ dependency, namespaceFullPath });
          }}
        >
          <Box display="flex" gap={2} flexDirection="column">
            <SelectGroup
              TextFieldProps={{ label: "Search within group" }}
              onChange={(_, fullPath) => {
                setNamespaceFullPath(fullPath);
              }}
            />
            <TextField
              sx={{ flexGrow: 1 }}
              label="Dependency name"
              value={dependency}
              onChange={(e) => {
                setDependency(e.target.value);
              }}
            />
            <Button
              type="submit"
              variant="contained"
              disabled={dependency === ""}
            >
              Go
            </Button>
          </Box>
        </form>
      </Box>
      {!!search && (
        <TableContainer component={Paper} sx={{ marginBottom: 2 }}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Project Path</TableCell>
                <TableCell>Dependency Location</TableCell>
                <TableCell>Version</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {(filteredProjects ?? []).map(
                ({
                  project: { fullPath, id },
                  dependency: {
                    version,
                    location: { path, blobPath },
                  },
                }) => (
                  <TableRow
                    key={id}
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {fullPath}
                    </TableCell>
                    <TableCell>
                      <Link
                        href={`https://gitlab.developers.cam.ac.uk/${blobPath}`}
                        target="_blank"
                      >
                        {path}
                      </Link>
                    </TableCell>
                    <TableCell>{version}</TableCell>
                  </TableRow>
                ),
              )}
              {showLoadingIndicator && (
                <>
                  <div ref={sentryRef} />
                  {new Array(3).fill(null).map((_, idx) => (
                    <TableRow
                      key={idx}
                      sx={{
                        "&:last-child td, &:last-child th": { border: 0 },
                      }}
                    >
                      <TableCell>
                        <Skeleton variant="text" />
                      </TableCell>
                      <TableCell>
                        <Skeleton variant="text" />
                      </TableCell>
                      <TableCell>
                        <Skeleton variant="text" />
                      </TableCell>
                    </TableRow>
                  ))}
                </>
              )}
            </TableBody>
          </Table>
        </TableContainer>
      )}
    </Page>
  );
};

export default Dependencies;
