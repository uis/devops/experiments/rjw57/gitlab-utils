import { useMemo, useState } from "react";
import CircularProgress from "@mui/material/CircularProgress";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Typography from "@mui/material/Typography";
import { gql } from "graphql-request";

import useGraphQLQuery from "../hooks/useGraphQLQuery";
import SelectProject, { ProjectSearchQueryNode } from "./SelectProject";
import Page from "./Page";

interface ApprovalRulesQueryData {
  project: {
    id: string;
    branchRules: {
      nodes: {
        approvalRules: {
          nodes: {
            name: string;
            id: string;
          }[];
        };
      }[];
    };
  };
}

const approvalRulesQuery = gql`
  query approvalRules($fullPath: ID!) {
    project(fullPath: $fullPath) {
      id
      branchRules {
        nodes {
          approvalRules {
            nodes {
              name
              id
            }
          }
        }
      }
    }
  }
`;

export const ApprovalRules = () => {
  const [projectNode, setProjectNode] = useState<ProjectSearchQueryNode | null>(
    null,
  );
  const { data, isLoading } = useGraphQLQuery<ApprovalRulesQueryData>(
    approvalRulesQuery,
    {
      fullPath: projectNode?.fullPath,
    },
    { enabled: !!projectNode?.fullPath },
  );
  const approvalRules = useMemo(
    () =>
      data?.project.branchRules.nodes
        .map((node) => node.approvalRules.nodes ?? [])
        .flat(),
    [data],
  );

  return (
    <Page title="Project approval rules">
      <Typography variant="body1" sx={{ marginBottom: 2 }}>
        Allows one to determine the numeric ids of any existing approval rules
        for a project. Useful when <code>terraform import</code>-ing projects
        into GitLab project factory.
      </Typography>
      <SelectProject
        TextFieldProps={{ fullWidth: true }}
        onChange={(_e, _value, node) => {
          setProjectNode(node);
        }}
      />
      {projectNode && (
        <Paper sx={{ padding: 2, marginTop: 2 }}>
          <Typography variant="body1">{projectNode.name}</Typography>
          <Typography variant="body2" color="text.secondary">
            {projectNode.id}
          </Typography>
        </Paper>
      )}

      {projectNode &&
        !isLoading &&
        (!approvalRules || approvalRules.length === 0) && (
          <Typography
            variant="body1"
            color="text.secondary"
            component={Paper}
            sx={{ marginTop: 2, padding: 2, textAlign: "center" }}
          >
            No approval rules
          </Typography>
        )}
      {projectNode && isLoading && (
        <Typography
          variant="body1"
          component={Paper}
          sx={{ marginTop: 2, padding: 2, textAlign: "center" }}
        >
          <CircularProgress color="inherit" size={20} />
        </Typography>
      )}
      {projectNode &&
        !isLoading &&
        approvalRules &&
        approvalRules.length > 0 && (
          <TableContainer component={Paper} sx={{ marginTop: 2 }}>
            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>ID</TableCell>
                  <TableCell>Name</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {approvalRules.map(({ id, name }) => (
                  <TableRow
                    key={name}
                    sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                  >
                    <TableCell component="th" scope="row">
                      {id}
                    </TableCell>
                    <TableCell>{name}</TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        )}
    </Page>
  );
};

export default ApprovalRules;
