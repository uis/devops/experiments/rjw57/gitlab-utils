import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton, {
  ListItemButtonProps,
} from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";

import { Link as RouterLink } from "react-router-dom";

interface ListItemLinkButtonProps extends ListItemButtonProps {
  to: string;
  replace?: boolean;
}

const ListItemLinkButton = ({
  to,
  replace,
  ...props
}: ListItemLinkButtonProps) => (
  <ListItemButton
    component={RouterLink as any}
    to={to}
    replace={replace}
    {...props}
  />
);

export const NavPane = () => (
  <>
    <List>
      <ListItem disablePadding>
        <ListItemLinkButton to="/">
          <ListItemText primary="Home" />
        </ListItemLinkButton>
      </ListItem>
      <ListItem disablePadding>
        <ListItemLinkButton to="/approval-rules">
          <ListItemText primary="Approval rules" />
        </ListItemLinkButton>
      </ListItem>
      <ListItem disablePadding>
        <ListItemLinkButton to="/dependencies">
          <ListItemText primary="Dependencies" />
        </ListItemLinkButton>
      </ListItem>
      <ListItem disablePadding>
        <ListItemLinkButton to="/unmanaged-projects">
          <ListItemText primary="Unmanaged Projects" />
        </ListItemLinkButton>
      </ListItem>
      <ListItem disablePadding>
        <ListItemLinkButton to="/generate-factory-config">
          <ListItemText primary="Generate Factory Config" />
        </ListItemLinkButton>
      </ListItem>
      <ListItem disablePadding>
        <ListItemLinkButton to="/technology-stats">
          <ListItemText primary="Technology Use Statistics" />
        </ListItemLinkButton>
      </ListItem>
    </List>
  </>
);

export default NavPane;
