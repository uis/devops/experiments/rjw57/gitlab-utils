import { request } from "graphql-request";
import { gql } from "graphql-request";

import { GRAPHQL_ENDPOINT } from "../config";

const technologiesQuery = gql`
  query tecnhologies($groupFullPath: ID!, $after: String) {
    group(fullPath: $groupFullPath) {
      projects(includeSubgroups: true, after: $after) {
        nodes {
          fullPath
          languages {
            name
          }
          dependencies(
            packageManagers: [
              BUNDLER
              YARN
              NPM
              PNPM
              MAVEN
              COMPOSER
              PIP
              CONAN
              GO
              NUGET
              SBT
              GRADLE
              PIPENV
              POETRY
              SETUPTOOLS
              CONDA
            ]
          ) {
            nodes {
              name
              packager
            }
            pageInfo {
              hasNextPage
              endCursor
            }
          }
        }
        pageInfo {
          hasNextPage
          endCursor
        }
      }
    }
  }
`;

interface TechnologiesQueryData {
  group: {
    projects: {
      nodes: {
        fullPath: string;
        languages: {
          name: string;
        }[];
        dependencies: {
          nodes: {
            name: string;
            packager: string;
          }[];
          pageInfo: {
            hasNextPage: boolean;
            endCursor: string | null;
          };
        } | null;
      }[];
      pageInfo: {
        hasNextPage: boolean;
        endCursor: string | null;
      };
    };
  };
}

const projectDetailQuery = gql`
  query projectDetail($projectFullPath: ID!, $after: String) {
    project(fullPath: $projectFullPath) {
      dependencies(
        packageManagers: [
          BUNDLER
          YARN
          NPM
          PNPM
          MAVEN
          COMPOSER
          PIP
          CONAN
          GO
          NUGET
          SBT
          GRADLE
          PIPENV
          POETRY
          SETUPTOOLS
          CONDA
        ]
        after: $after
      ) {
        pageInfo {
          hasNextPage
          endCursor
        }
        nodes {
          name
          packager
        }
      }
    }
  }
`;

interface ProjectDetailQueryData {
  project: {
    dependencies: {
      nodes: {
        name: string;
        packager: string;
      }[];
      pageInfo: {
        hasNextPage: boolean;
        endCursor: string | null;
      };
    };
  };
}

export interface ProjectRecord {
  fullPath: string;
  languages: string[];
  dependencies: {
    packager: string;
    name: string;
  }[];
}

const fetchAdditionalDependencies = async (
  token: string,
  projectFullPath: string,
  after: string | null,
  target: ProjectRecord["dependencies"],
  logMessage: (s: string) => void,
) => {
  while (true) {
    logMessage(`Fetching more dep(s) for ${projectFullPath} after ${after}`);
    const {
      project: {
        dependencies: {
          nodes,
          pageInfo: { hasNextPage, endCursor },
        },
      },
    }: ProjectDetailQueryData = await request<ProjectDetailQueryData>(
      GRAPHQL_ENDPOINT,
      projectDetailQuery,
      { projectFullPath, after },
      {
        Authorization: `Bearer ${token}`,
      },
    );
    nodes.forEach(({ name, packager }) => {
      target.push({ name, packager });
    });
    after = endCursor;
    if (!hasNextPage) {
      break;
    }
  }
};

interface FetchTechnologiesOptions {
  logMessage?: (message: string) => void;
}

export const fetchTechnologies = async (
  token: string,
  groupFullPath: string,
  options?: FetchTechnologiesOptions,
) => {
  let after: string | null = null;
  const allProjects: ProjectRecord[] = [];
  const extraFetches: Promise<any>[] = [];
  const { logMessage } = {
    logMessage: (s: string) => {
      console.log(s);
    },
    ...options,
  };
  while (true) {
    logMessage(`Fetching projects for ${groupFullPath} after ${after}`);
    const {
      group: {
        projects: {
          nodes,
          pageInfo: { hasNextPage, endCursor },
        },
      },
    }: TechnologiesQueryData = await request<TechnologiesQueryData>(
      GRAPHQL_ENDPOINT,
      technologiesQuery,
      { groupFullPath, after },
      {
        Authorization: `Bearer ${token}`,
      },
    );

    nodes.forEach(({ languages, dependencies, fullPath }) => {
      allProjects.push({
        fullPath,
        languages: languages.map(({ name }) => name),
        dependencies: dependencies ? dependencies.nodes : [],
      });
      if (dependencies?.pageInfo.hasNextPage) {
        console.log(`Fetching additional dependencies for ${fullPath}`);
        extraFetches.push(
          fetchAdditionalDependencies(
            token,
            fullPath,
            dependencies.pageInfo.endCursor,
            dependencies?.nodes,
            logMessage,
          ),
        );
      }
    });

    after = endCursor;
    if (!hasNextPage) {
      break;
    }
  }

  logMessage(
    `Waiting for any additional project dependency fetches for ${groupFullPath}.`,
  );
  await Promise.all(extraFetches);
  logMessage(
    `Finished for group ${groupFullPath}. Fetched ${allProjects.length} project(s) in total.`,
  );

  return allProjects;
};
