import { request, RequestDocument, Variables } from "graphql-request";
import {
  useInfiniteQuery,
  UseInfiniteQueryOptions,
} from "@tanstack/react-query";

import useGitLabAuth from "./useGitLabAuth";
import { GRAPHQL_ENDPOINT } from "../config";

export interface UseInfiniteGraphQLQueryOptions<Data>
  extends Omit<
    UseInfiniteQueryOptions<Data>,
    | "queryFn"
    | "queryKey"
    | "getNextPageParam"
    | "getPreviousPageParam"
    | "initialPageParam"
    | "select" // FIXME: I don't know why we need to omit this to typecheck, but we do.
  > {
  cursorVariableName?: string;
}

export const useInfiniteGraphQLQuery = <Data, PageParam = string>(
  document: RequestDocument,
  getNextCursor: (page: Data) => PageParam | undefined,
  variables?: Variables,
  options?: UseInfiniteGraphQLQueryOptions<Data>,
) => {
  const { cursorVariableName, ...useInfiniteQueryOptions } = {
    cursorVariableName: "after",
    ...options,
  };
  const { token } = useGitLabAuth();
  return useInfiniteQuery<Data>({
    // We use GRAPHQL_ENDPOINT and token in the key so that we don't cache results between different
    // users.
    queryKey: [GRAPHQL_ENDPOINT, token, document, variables],
    queryFn: ({ pageParam }) =>
      request<Data>(
        GRAPHQL_ENDPOINT,
        document,
        { [cursorVariableName]: pageParam, ...variables },
        {
          Authorization: `Bearer ${token}`,
        },
      ),
    initialPageParam: null,
    getNextPageParam: (lastPage) => getNextCursor(lastPage),
    ...useInfiniteQueryOptions,
  });
};

export default useInfiniteGraphQLQuery;
