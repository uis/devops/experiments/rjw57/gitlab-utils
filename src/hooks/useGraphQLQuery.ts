import { request, RequestDocument, Variables } from "graphql-request";
import { useQuery, UseQueryOptions } from "@tanstack/react-query";

import useGitLabAuth from "./useGitLabAuth";
import { GRAPHQL_ENDPOINT } from "../config";

export const useGraphQLQuery = <Data>(
  document: RequestDocument,
  variables?: Variables,
  options?: Omit<UseQueryOptions<Data>, "queryFn" | "queryKey">,
) => {
  const { token } = useGitLabAuth();
  return useQuery<Data>({
    // We use GRAPHQL_ENDPOINT and token in the key so that we don't cache results between different
    // users.
    queryKey: [GRAPHQL_ENDPOINT, token, document, variables],
    queryFn: () =>
      request<Data>(GRAPHQL_ENDPOINT, document, variables, {
        Authorization: `Bearer ${token}`,
      }),
    ...options,
  });
};

export default useGraphQLQuery;
