import { request, RequestDocument, Variables } from "graphql-request";
import { useQuery, UseQueryOptions } from "@tanstack/react-query";

import useGitLabAuth from "./useGitLabAuth";
import { GRAPHQL_ENDPOINT } from "../config";

export const usePaginatedGraphQLQuery = <Data>(
  document: RequestDocument,
  variables?: Variables,
  options?: Omit<UseQueryOptions<Data[]>, "queryFn" | "queryKey"> & {
    nextPageVariablesFn?: (data: Data) => Variables | null;
  },
) => {
  const { token } = useGitLabAuth();
  return useQuery<Data[]>({
    // We use GRAPHQL_ENDPOINT and token in the key so that we don't cache results between different
    // users.
    queryKey: [GRAPHQL_ENDPOINT, token, document, variables],
    queryFn: async () => {
      const pages: Data[] = [];
      let pageVariables: Variables = { ...variables };
      while (true) {
        const nextPage = await request<Data>(
          GRAPHQL_ENDPOINT,
          document,
          pageVariables,
          {
            Authorization: `Bearer ${token}`,
          },
        );
        pages.push(nextPage);
        if (options?.nextPageVariablesFn) {
          const newVariables = options.nextPageVariablesFn(nextPage);
          if (!newVariables) {
            break;
          }
          pageVariables = { ...pageVariables, ...newVariables };
        } else {
          break;
        }
      }

      return pages;
    },
    ...options,
  });
};

export default usePaginatedGraphQLQuery;
