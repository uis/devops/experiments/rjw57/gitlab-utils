import { useContext } from "react";
import {
  AuthContext,
  IAuthContext,
} from "react-oauth2-code-pkce";

export const useGitLabAuth = () => useContext<IAuthContext>(AuthContext);

export default useGitLabAuth;
